<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Service;
use App\Models\User;
use App\Models\ServiceOrders;
use Illuminate\Support\Facades\Validator;
use App\CentralLogics\Helpers;
use App\Models\DeliveryMan;
use App\Models\ServiceComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ServiceOrderController extends Controller
{

    public function place_order(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'service_id' => 'required',
            'details' => 'required',
            'address_id' => 'required',
            'address' => 'required',

        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => Helpers::error_processor($validator)], 403);
        }


        if (Auth::guard('api')) {
            $user_id = $request->user()->id;
            $user_image = $request->user()->image;
            $user_name = $request->user()->f_name .' '. $request->user()->l_name;
        } else {
            $user_id = 0;
        }
        if ($request->has('image')) {
            $image_name = Helpers::upload('serviceorder/', 'png', $request->file('image'));
        } else {
            $image_name = null;
        }

        ServiceOrders::create([
            'service_id' => $request->service_id,
            'details' => $request->details,
            'user_id' => $user_id,
            'user_image' => $user_image,
            'user_name' => $user_name,
            'image' => $image_name,
            'address_id' => $request->address_id,
            'address' => $request->address,
            'status' => 'pending',
        ]);

        return response()->json(['Service order has been placed'], 200);
    }
    public function orders(Request $request)
    {

        if (Auth::guard('api')) {
            $user_id = $request->user()->id;
        } else {
            $user_id = 0;
        }

        try {
            $user = User::where('id', $user_id)->get(['f_name', 'l_name']);
            $orders = ServiceOrders::where('user_id', $user_id)->orderBy('id', 'desc')->get(['id','address', 'status', 'image', 'details', 'service_id']);

            return response()->json([$user, $orders], 200);
        } catch (\Exception $e) {
            return response()->json([], 200);
        }
    }

    public function comment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token'=>'required',
            'order_id' => 'required',
            'comment' => 'required',

        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => Helpers::error_processor($validator)], 403);
        }
        $dm = DeliveryMan::where(['auth_token' => $request->token])->first();

       


        ServiceComment::create([
            'order_id' => $request->order_id,
            'comment' => $request->comment,
            'user_lname' => $dm->l_name,
            'user_id' => $dm->id,
            'image' => $dm->image,
            'user_fname' => $dm->f_name,
        ]);

        return response()->json(['comment has been placed'], 200);
    }

    public function post_comment($id)
    {
        try {

            $comment = ServiceComment::where('order_id', $id)->get();
            return response()->json([$comment], 200);
        } catch (\Exception $e) {
            return response()->json([], 200);
        }
    }




    public function edit(Request $request,$id)
    {

        $comment=ServiceComment::find($id);
        $validator = Validator::make($request->all(), [
            'token'=>'required', 
            'comment' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => Helpers::error_processor($validator)], 403);
        }
        $dm = DeliveryMan::where(['auth_token' => $request->token])->first();

       

       if ($comment->user_id==$dm->id){
       $comment->update([
         
            'comment' => $request->comment,
            
        ]);
        return response()->json(['comment has been updated'], 200);
    }else{
        return response()->json(['u cant edit this comment'], 404);
    }
       
    }

    public function order($token)
    {

       
        $dm = DeliveryMan::where(['auth_token' =>$token])->first();

        try {
            
            $orders = ServiceOrders::where('service_id', $dm->service_id)->orderBy('id', 'desc')->get(['id','address','user_image','user_name', 'status', 'image', 'details', 'service_id','user_id']);

            return response()->json([$orders], 200);
        } catch (\Exception $e) {
            return response()->json([], 200);
        }
    }


    public function delete(Request $request,$id)
    {

        $comment=ServiceComment::find($id);
        $validator = Validator::make($request->all(), [
            'token'=>'required', 
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => Helpers::error_processor($validator)], 403);
        }
        $dm = DeliveryMan::where(['auth_token' => $request->token])->first();

       

       if ($comment->user_id==$dm->id){
       $comment->delete();
        return response()->json(['comment has been deleted'], 200);
    }else{
        return response()->json(['u cant delete this comment'], 404);
    }
       
    }
}
