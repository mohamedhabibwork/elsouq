<?php
namespace App\Http\Controllers\Api\V1;
use App\Http\Controllers\Controller;
use App\Models\Service;
use App\Models\Translation;

class ServiceController extends Controller
{

public function get_services()
    {
        try {
            $services = Service::where(['status'=>1])->get();
            $arabic_name=Translation::where('translationable_type','App\Models\Service')->get(['translationable_id','value']);
            return response()->json([$services,$arabic_name], 200);
        } catch (\Exception $e) {
            return response()->json(['failed'],400);
        }
    }
}