<?php

namespace App\Http\Controllers;

use App\CentralLogics\Helpers;
use App\Mail\SelfRegistration;
use App\Models\BusinessSetting;
use App\Models\DeliveryMan;
use App\Models\Service;
use Brian2694\Toastr\Facades\Toastr;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class DeliveryManController extends Controller
{
    public function create()
    {
        $status = BusinessSetting::where('key', 'toggle_dm_registration')->first();
        if (!isset($status) || $status->value == '0') {
            Toastr::error(trans('messages.not_found'));
            return back();
        }

        return view('dm-registration');
    }

    public function service()
    {
        $services = Service::get();

        return view('service-registration', compact('services'));
    }

    public function store(Request $request)
    {
        $status = BusinessSetting::where('key', 'toggle_dm_registration')->first();
        if (!isset($status) || $status->value == '0') {
            Toastr::error(trans('messages.not_found'));
            return back();
        }

        $request->validate([
            'f_name' => 'required|max:100',
            'l_name' => 'nullable|max:100',
            'identity_number' => 'required|max:30',
            'email' => 'required|email|unique:delivery_men',
            'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|unique:delivery_men',
            'zone_id' => 'required',
            'earning' => 'required',
            'password' => 'required|min:6',
        ], [
            'f_name.required' => trans('messages.first_name_is_required'),
            'zone_id.required' => trans('messages.select_a_zone'),
            'earning.required' => trans('messages.select_dm_type')
        ]);

        if ($request->has('image')) {
            $image_name = Helpers::upload('delivery-man/', 'png', $request->file('image'));
        } else {
            $image_name = 'def.png';
        }

        $id_img_names = [];
        if (!empty($request->file('identity_image'))) {
            foreach ($request->identity_image as $img) {
                $identity_image = Helpers::upload('delivery-man/', 'png', $img);
                $id_img_names[] = $identity_image;
            }
            $identity_image = json_encode($id_img_names);
        } else {
            $identity_image = json_encode([]);
        }

        $dm = new DeliveryMan();
        $dm->f_name = $request->f_name;
        $dm->l_name = $request->l_name;
        $dm->email = $request->email;
        $dm->phone = $request->phone;
        $dm->identity_number = $request->identity_number;
        $dm->identity_type = $request->identity_type;
        $dm->zone_id = $request->zone_id;
        $dm->identity_image = $identity_image;
        $dm->image = $image_name;
        $dm->active = 0;
        $dm->earning = $request->earning;
        $dm->password = bcrypt($request->password);
        $dm->application_status = 'pending';
        $dm->save();
        try {
            if (config('mail.status')) {
                Mail::to($request['email'])->send(new SelfRegistration('pending',
                    $dm->f_name . ' ' . $dm->l_name));
            }
        } catch (Exception $ex) {
            info($ex);
        }

        Toastr::success(trans('messages.application_placed_successfully'));
        return back();
    }


    public function stores(Request $request)
    {
        $dm = $this->getDeliveryMan($request);
        $dm->save();

        Toastr::success(trans('messages.added successfully'));
        return redirect('serviceprovider/apply');
    }

    /**
     * @param Request $request
     * @return DeliveryMan
     */
    public function getDeliveryMan(Request $request): DeliveryMan
    {
        if ($request->has('service_id')) {
            $request->validate(
                [
                    'f_name' => 'required|max:100',
                    'l_name' => 'nullable|max:100',
                    'email' => 'required|unique:delivery_men',
                    'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:20|unique:delivery_men',
                    'password' => 'required|min:6',
                ],
                [
                    'f_name.required' => trans('messages.first_name_is_required'),
                ]
            );
        } else {
            $request->validate([
                'f_name' => 'required|max:100',
                'l_name' => 'nullable|max:100',
                'identity_number' => 'nullable|max:30',
                'email' => 'required|unique:delivery_men',
                'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:20|unique:delivery_men',
                'zone_id' => 'required',
                'earning' => 'required',
                'password' => 'required|min:6',
            ], [
                'f_name.required' => trans('messages.first_name_is_required'),
                'zone_id.required' => trans('messages.select_a_zone'),
                'earning.required' => trans('messages.select_dm_type')
            ]);
        }


        if ($request->has('image')) {
            $image_name = Helpers::upload('delivery-man/', 'png', $request->file('image'));
        } else {
            $image_name = 'def.png';
        }

        if ($request->has('residence_image')) {
            $residence_image = Helpers::upload('delivery-man/', 'png', $request->file('residence_image'));
        }
        if ($request->has('licence_image')) {
            $licence_image = Helpers::upload('delivery-man/', 'png', $request->file('licence_image'));
        }
        if ($request->has('form_image')) {
            $form_image = Helpers::upload('delivery-man/', 'png', $request->file('form_image'));
        }

        $id_img_names = [];
        if (!empty($request->file('identity_image'))) {
            foreach ($request->identity_image as $img) {
                $identity_image = Helpers::upload('delivery-man/', 'png', $img);
                $id_img_names[] = $identity_image;
            }
            $identity_image = json_encode($id_img_names);
        } else {
            $identity_image = json_encode([]);
        }

        $dm = new DeliveryMan();
        $dm->f_name = $request->f_name;
        $dm->l_name = $request->l_name;
        $dm->email = $request->email;
        $dm->phone = $request->phone;
        $dm->service_id = $request->service_id;
        $dm->identity_number = $request->identity_number;
        $dm->identity_type = $request->identity_type;
        $dm->zone_id = $request->zone_id;
        if ($request->has('expiry_date')) {
            $dm->expiry_date = $request->expiry_date;
        }

        $dm->identity_image = $identity_image;
        $dm->image = $image_name;
        if ($request->has('residence_image')) {
            $dm->residence_image = $residence_image;
        }
        if ($request->has('licence_image')) {
            $dm->licence_image = $licence_image;
        }
        if ($request->has('form_image')) {
            $dm->form_image = $form_image;
        }
        if ($request->has('earning')) {
            $dm->earning = $request->earning;
        }

        if ($request->has('service_id')) {
            $dm->active = 1;
        } else {
            $dm->active = 0;
        }

        $dm->password = bcrypt($request->password);
        return $dm;
    }
}
