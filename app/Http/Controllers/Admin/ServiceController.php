<?php

namespace App\Http\Controllers\Admin;

use App\CentralLogics\Helpers;
use App\Http\Controllers\Controller;
use App\Models\Service;
use App\Models\ServiceOrders;
use App\Models\Translation;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Database\Eloquent\Models;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ServiceController extends Controller
{
    function index(Request $request)
    {
        $query_param = [];
        $search = $request['search'];
        if ($request->has('search')) {
            $key = explode(' ', $request['search']);

            $query_param = ['search' => $request['search']];
        } else {
        }
        $services = Service::get();
        // dd($services);
        $arabic_name = Translation::where('translationable_type', 'App\Models\Service')->get(['translationable_id', 'value']);

        return view('admin-views.service.index', compact('services', 'search', 'arabic_name'));
    }


    public function search(Request $request)
    {
        $key = explode(' ', $request['search']);
        $services = Service::where(function ($q) use ($key) {
            foreach ($key as $value) {
                $q->orWhere('name', 'like', "%{$value}%");
            }
        })->get();
        return response()->json([
            'view' => view('admin-views.service.partials._table', compact('services'))->render()
        ]);
    }




    function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        foreach ($request->name as $name) {
            if (strlen($name) > 255) {
                toastr::error(translate('Name is too long!'));
                return back();
            }
        }

        //uniqueness check
        $serv = Service::where('name', $request->name)->first();


        //image upload
        if (!empty($request->file('image'))) {

            $image_name = Helpers::upload('service/', 'png', $request->file('image'));
        } else {
            $image_name = 'def.png';
        }

        //into db
        $service = new Service();
        $service->name = $request->name[array_search('en', $request->lang)];
        $service->image = $image_name;
        $service->save();

        //translation
        $data = [];
        foreach ($request->lang as $index => $key) {
            if ($request->name[$index] && $key != 'en') {
                array_push($data, array(
                    'translationable_type' => 'App\Models\Service',
                    'translationable_id' => $service->id,
                    'locale' => $key,
                    'key' => 'name',
                    'value' => $request->name[$index],
                ));
            }
        }
        if (count($data)) {
            Translation::insert($data);
        }

        Toastr::success(translate('Service Added Successfully!'));
        return back();
    }

    public function edit($id)
    {
        $service = Service::withoutGlobalScopes()->with('translations')->find($id);
        $arabic_name = Translation::where('translationable_type', 'App\Models\Service')->where('translationable_id', $service->id)->get();
        $ids = [];
        foreach ($arabic_name as $item) {
            array_push($ids, $item->value);
        }

        return view('admin-views.service.edit', compact('service', 'ids'));
    }

    public function status(Request $request)
    {
        $service = Service::find($request->id);
        $service->status = $request->status;
        $service->save();
        Toastr::success(translate('service status updated!'));
        return back();
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);

        foreach ($request->name as $name) {
            if (strlen($name) > 255) {
                toastr::error(translate('Name is too long!'));
                return back();
            }
        }

        $service = Service::find($id);
        $service->name = $request->name[array_search('en', $request->lang)];
        $service->image = $request->has('image') ? Helpers::update('service/', $service->image, 'png', $request->file('image')) : $service->image;
        $service->save();
        foreach ($request->lang as $index => $key) {
            if ($request->name[$index] && $key != 'en') {
                Translation::updateOrInsert(
                    [
                        'translationable_type' => 'App\Models\Service',
                        'translationable_id' => $service->id,
                        'locale' => $key,
                        'key' => 'name'
                    ],
                    ['value' => $request->name[$index]]
                );
            }
        }
        Toastr::success(translate('service updated successfully!'));
        return back();
    }

    public function delete(Request $request)
    {
        $service = Service::find($request->id);
        if (Storage::disk('public')->exists('service/' . $service['image'])) {
            Storage::disk('public')->delete('service/' . $service['image']);
        }

        $service->delete();
        Toastr::success(translate('Service removed!'));

        return back();
    }
    public function user_index()
    {
        return view('admin-views.service.prov_index');
    }


    public function orders()
    {
        $orders = ServiceOrders::OrderBy('id', 'desc')->get();
        return view('admin-views.service.orders', compact('orders'));
    }
    public function order_delete($id)
    {
        $order = ServiceOrders::findOrFail($id);
        $order->delete();
        Toastr::success(translate('order deleted successfully!'));
        return back();
    }
    public function order_status($id)
    {
        $order = ServiceOrders::findOrFail($id);
        if ($order->status == 'pending') {
            $order->update([
                'status' => 'accepted',
            ]);
        } else {
            $order->update([
                'status' => 'pending',
            ]);
        }

        Toastr::success(translate('order status changed successfully!'));
        return back();
    }
}
