<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class ServiceComment extends Model
{
    protected $table = 'service_comments';
    use HasFactory;
    protected $fillable=['user_lname','user_id','comment','order_id','user_fname','image'];



    public function ServiceOrders()
    {
        return $this->belongsTo(ServiceOrders::class,'order_id');
    }
   
   

   


    
}