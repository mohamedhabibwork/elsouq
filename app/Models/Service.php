<?php




namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Models\DeliveryMan;
class Service extends Model
{
    protected $table = 'services';
    use HasFactory;
    public function translations()
    {
        return $this->morphMany('App\Models\Translation', 'translationable');
    }

    public function scopeActive($query)
    {
        return $query->where('status', '=', 1);
    }

    public function deliveryman()
    {
        return $this->hasMany(DeliveryMan::class,'service_id');
    }
    public function service_orders()
    {
        return $this->hasMany(ServiceOrders::class,'service_id');
    }

    // public function getNameAttribute($name)
    // {
    //     if (auth('admin')->check() || auth('branch')->check()) {
    //         return $name;
    //     }
    //     return $this->translations[0]->value ?? $name;
    // }

    protected static function booted()
    {
        static::addGlobalScope('translate', function (Builder $builder) {
            $builder->with(['translations' => function($query){
                return $query->where('locale', app()->getLocale());
            }]);
        });
    }
}
