<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;


class ServiceOrders extends Model
{
    protected $table = 'service_orders';
    use HasFactory;
    protected $fillable = ['service_id', 'address_id', 'user_id', 'details', 'image', 'address', 'status','user_image','user_name'];
    public function customer()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id');
    }
    public function comment()
    {
        return $this->hasMany(ServiceComment::class, 'order_id');
    }
}
