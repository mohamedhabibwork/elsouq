<?php

return [
    'searching_for_delivery_man'=>'البحث عن رجل التوصيل',
    'accepted_by_delivery_man'=>'قبلها رجل التوصيل',
    'preparing_in_restaurants'=>'التحضير في المطاعم',
    'picked_up'=>'التقط',
    'dashboard_order_statistics'=>'إحصائيات طلب لوحة القيادة',
];
